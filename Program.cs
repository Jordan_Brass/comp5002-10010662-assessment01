﻿using System;

namespace comp5002_10010662_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var name = "0";
            var numOne = 0.00;
            var confirm = "0";

            // Ask user for their name
            Console.WriteLine("What is your name?");
            // Store user input to a variable
            name = Console.ReadLine();
            // Ask user for a number with 2 decimal places
            Console.WriteLine($"Hello {name}. Please enter a number with at most 2 decimal places");
            // Store user input to a variable
            numOne = Convert.ToDouble(Console.ReadLine());

            Console.Clear();
            // Ask if the user would like to add more numbers
            Console.WriteLine("Would you like to enter another number?");
            Console.WriteLine("Y or N");
            // Get user input and store it to a confirmation variable
            confirm = Console.ReadLine();
            // Check user's answer
            if (confirm == "Y" || confirm == "y")
            {
                Console.Clear();
                // If they answer yes ask them for another number
                Console.WriteLine("Please enter a number with at most 2 decimal places");
                // Store the new number to a variable
                numOne += Convert.ToDouble(Console.ReadLine());    
            }
            else
            {
                // If they answer no skip to the next step
            }
            Console.Clear();
            // Give the user a total
            Console.WriteLine($"Your total (before GST) is {(numOne):C2}");
            Console.WriteLine("");
            // Add a pause for user input to continue
            Console.WriteLine("Press <ENTER> to continue and add GST");
            // Get user input
            Console.ReadKey();
            // Add GST to total and display to user
            Console.WriteLine($"Your new total (after GST) is {(numOne)*1.15:C2}");
            // Thank the user for using the store and say a farewell
            Console.WriteLine($"Goodbye {name} and thank you for shopping at my store");
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <ENTER> to quit the program");
            Console.ReadKey();
        }
    }
}
